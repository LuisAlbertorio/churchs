package com.chamas.luis.churchs2.Fragments;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.chamas.luis.churchs2.pdfViewerActivity;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class NutricionFragment extends Fragment {

    private FragmentActivity myContext;

    MainActivity act;
    TextView title;
    ImageView nutriImage;


    public NutricionFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Nutrición");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Nutrición");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }
        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }
//        activity.toolTitle.setText("Church");
        super.onDestroyView();
    }

    boolean nutripdf;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_nutricion, container, false);

        final TextView description = (TextView)rootView.findViewById(R.id.nutricionDescriptionTextView);
        final ImageButton downloadPDF = (ImageButton)rootView.findViewById(R.id.descargarNutricionPDF);
        title = (TextView)rootView.findViewById(R.id.nutricionTitleTextView);
        nutriImage = (ImageView)rootView.findViewById(R.id.nutricionImageView);

        nutripdf = getArguments().getBoolean("nutripdf");

        if(!nutripdf){
            downloadPDF.setVisibility(View.GONE);
        }

        HashMap hashMap = new HashMap();

        ParseCloud.callFunctionInBackground("getNutriciones", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {

            @Override
            public void done(final ArrayList<ParseObject> object, ParseException e) {
                description.setText(object.get(0).getString("description"));
                title.setText(object.get(0).getString("title"));
                Picasso.with(getActivity()).load(object.get(0).getParseFile("image").getUrl()).into(nutriImage);

                downloadPDF.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), pdfViewerActivity.class);
                        intent.putExtra("url",object.get(0).getParseFile("pdf").getUrl());
                        getActivity().startActivity(intent);
                    }
                });

            }
        });

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Banners");
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
//                Picasso.with(getActivity()).load(object.getParseFile("bannerNutricion").getUrl()).into(nutriImage);
            }
        });


        return rootView;
    }

    public void downloadFile(String fileUrl){

    }


}
