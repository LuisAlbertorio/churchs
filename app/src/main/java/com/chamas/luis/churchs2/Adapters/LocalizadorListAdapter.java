package com.chamas.luis.churchs2.Adapters;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.MapsActivity;
import com.chamas.luis.churchs2.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Luis on 5/25/2016.
 */
public class LocalizadorListAdapter extends BaseAdapter {

    Context context;
    String [] result;
    private static LayoutInflater inflater=null;
    ArrayList<ParseObject> objects;
    Location myLoc;

    public LocalizadorListAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(ArrayList<ParseObject> objects, Location myLoc){
        this.objects = objects;
        this.myLoc = myLoc;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return (objects == null) ? 1 : objects.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(position == 0){
            View rowView = inflater.inflate(R.layout.localepicture, parent, false);

            final ImageView imageView = (ImageView)rowView.findViewById(R.id.bannerLocalizador);

            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Banners");
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    Picasso.with(context).load(object.getParseFile("bannerLocalidad").getUrl()).into(imageView);
                }
            });
            return rowView;
        }else {

            View rowView = inflater.inflate(R.layout.custom_row_local, parent, false);

            TextView where = (TextView) rowView.findViewById(R.id.localizadorNameTextView);
            TextView num = (TextView) rowView.findViewById(R.id.positionNumber);
            TextView phone = (TextView)rowView.findViewById(R.id.localizadorNumberTextView);
            TextView distance = (TextView)rowView.findViewById(R.id.localizadorDistanceTextView);

            where.setText(objects.get(position-1).getString("name"));
            num.setText(String.valueOf(position));
            phone.setText(objects.get(position-1).getString("phone"));

            Location loc = new Location("");
            loc.setLatitude(objects.get(position-1).getParseGeoPoint("point").getLatitude());
            loc.setLongitude(objects.get(position-1).getParseGeoPoint("point").getLongitude());

            Log.d("myloc", String.valueOf(myLoc));
            Log.d("lat", String.valueOf(objects.get(position-1).getParseGeoPoint("point").getLatitude()));
            Log.d("lon", String.valueOf(objects.get(position-1).getParseGeoPoint("point").getLongitude()));

            final float dist = (float) (myLoc.distanceTo(loc) * 0.000621371);

            distance.setText(String.valueOf(String.format("%.1f", dist)) + " millas");


            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
//                    Toast.makeText(context2, "You Clicked " + result[position-1], Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context, MapsActivity.class);
                    intent.putExtra("name", objects.get(position-1).getString("name"));
                    intent.putExtra("phone", objects.get(position-1).getString("phone"));
                    intent.putExtra("distance", String.valueOf(String.format("%.1f", dist)));
                    intent.putExtra("address", objects.get(position-1).getString("address"));
                    intent.putExtra("lunes", objects.get(position-1).getString("lunes"));
                    intent.putExtra("martes", objects.get(position-1).getString("martes"));
                    intent.putExtra("miercoles", objects.get(position-1).getString("miercoles"));
                    intent.putExtra("jueves", objects.get(position-1).getString("jueves"));
                    intent.putExtra("viernes", objects.get(position-1).getString("viernes"));
                    intent.putExtra("sabado", objects.get(position-1).getString("sabado"));
                    intent.putExtra("domingo", objects.get(position-1).getString("domingo"));
                    intent.putExtra("lat", objects.get(position-1).getParseGeoPoint("point").getLatitude());
                    intent.putExtra("lon", objects.get(position-1).getParseGeoPoint("point").getLongitude());

                    Log.d("lat", String.valueOf(objects.get(position-1).getParseGeoPoint("point").getLatitude()));
                    Log.d("lon", String.valueOf(objects.get(position-1).getParseGeoPoint("point").getLongitude()));

                    context.startActivity(intent);
                }
            });

            return rowView;
        }

    }
}
