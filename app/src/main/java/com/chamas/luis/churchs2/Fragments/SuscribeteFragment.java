package com.chamas.luis.churchs2.Fragments;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class SuscribeteFragment extends Fragment {

    private FragmentActivity myContext;
    MainActivity act;
    EditText nombre, email;
    TextView fecha;
    ImageButton suscribete;
    CheckBox checkBox;

    public SuscribeteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Suscríbete");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Suscríbete");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }
        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }
//        activity.toolTitle.setText("Church");
        super.onDestroyView();
    }

    TextView suscribeteInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_suscribete, container, false);

        nombre = (EditText)rootView.findViewById(R.id.SuscribeteNombreEditText);
        fecha = (TextView) rootView.findViewById(R.id.SuscribeteFechaEditText);
        email = (EditText)rootView.findViewById(R.id.SuscribeteEmailEditText);
        suscribete = (ImageButton) rootView.findViewById(R.id.suscribeteButton);
        checkBox = (CheckBox)rootView.findViewById(R.id.checkBox);
        suscribeteInfo = (TextView)rootView.findViewById(R.id.suscribeteInfoTextView);

        ParseQuery<ParseObject> query = new ParseQuery<>("Copies");
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                suscribeteInfo.setText(object.getString("suscripcionInfo"));
                checkBox.setText(object.getString("suscripcionCheckbox"));
            }
        });

        fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog_fecha_nacimiento);

                Button cancelar = (Button)dialog.findViewById(R.id.cancelarButton);
                Button confirmar = (Button)dialog.findViewById(R.id.confirmarButton);

                final DatePicker datePicker = (DatePicker)dialog.findViewById(R.id.datePicker);
                datePicker.setSpinnersShown(true);
                datePicker.setCalendarViewShown(false);

                dialog.show();

                cancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                confirmar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Calendar calendar = Calendar.getInstance();
//                        calendar.set(datePicker.getDayOfMonth(), datePicker.getMonth(), datePicker.getYear());

                        Date date = new Date();
                        date.setMonth(datePicker.getMonth());
                        date.setDate(datePicker.getDayOfMonth());
                        date.setYear(datePicker.getYear()-1900);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String dateString = dateFormat.format(date);

                        fecha.setText(dateString);
                        dialog.dismiss();


                    }
                });


            }
        });

        suscribete.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                if(Objects.equals(nombre.getText().toString(), "") || Objects.equals(email.getText().toString(), "")){
                    Toast.makeText(getActivity(), "Favor de llenar todos los campos requeridos", Toast.LENGTH_LONG).show();
                }else{

                    ParseObject object = new ParseObject("Suscribete");
                    object.put("nombre", nombre.getText().toString());
                    object.put("email", email.getText().toString());
                    object.put("fechaNacimiento", fecha.getText().toString());
                    object.put("platform", "android");

                    if(checkBox.isChecked()){
                        object.put("checkbox", true);
                    }else{
                        object.put("checkbox", false);
                    }

                    object.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            act.graciasSub();
                        }
                    });

                }
            }
        });

        return rootView;
    }


}
