package com.chamas.luis.churchs2.Fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuienesFragment extends Fragment {
    private FragmentActivity myContext;

    ImageView missionArrow, visionArrow, trayecArrow;

    MainActivity act;


    public QuienesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Quiénes Somos");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Quiénes Somos");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }
        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }
//        activity.toolTitle.setText("Church");
        super.onDestroyView();
    }

    TextView misionTitle, misionText, guiasTitle, guiasText, histTitle, histText;
    ImageView banner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootview = inflater.inflate(R.layout.fragment_quienes, container, false);

        misionTitle = (TextView)rootview.findViewById(R.id.quienesSomosMisionTitle);
        misionText = (TextView)rootview.findViewById(R.id.quienesSomosMisionText);
        guiasTitle = (TextView)rootview.findViewById(R.id.quienesSomosGuiasTitle);
        guiasText = (TextView)rootview.findViewById(R.id.quienesSomosGuiasText);
        histTitle = (TextView)rootview.findViewById(R.id.quienesSomosHistoriaTitle);
        histText = (TextView)rootview.findViewById(R.id.quienesSomosHistotiaText);
        banner = (ImageView)rootview.findViewById(R.id.bannerQuienesSomos);

        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("Quienes");
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                misionTitle.setText(objects.get(0).getString("title"));
                misionText.setText(objects.get(0).getString("info"));
                guiasTitle.setText(objects.get(1).getString("title"));

                String[] info = objects.get(1).getString("info").split(",");

                Log.d("SPLIT", String.valueOf(info[0]));
                String newInfo = "";
                for(int i =0; i<info.length;i++){
                        newInfo =  newInfo + "• " + info[i] + "\n";
                }

                guiasText.setText(newInfo);
                histTitle.setText(objects.get(2).getString("title"));
                histText.setText(objects.get(2).getString("info"));
            }
        });

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Banners");
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                Picasso.with(getActivity()).load(object.getParseFile("bannerQuienes").getUrl()).into(banner);
            }
        });

        return rootview;
    }
}
