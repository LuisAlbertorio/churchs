package com.chamas.luis.churchs2.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Luis on 6/27/2016.
 */
public class MenuGridAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> names;
    ArrayList<String> urls;
    ArrayList<String> objIds;
    private static LayoutInflater inflater=null;
    MainActivity mainActivity;
    ArrayList<String> myObj;
    boolean nutripdf;

    public MenuGridAdapter(Context context, ArrayList<String> names, ArrayList<String> urls, ArrayList<String> objIds, boolean nutripdf){
        this.context = context;
        this.names = names;
        this.urls = urls;
        this.objIds = objIds;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mainActivity = (MainActivity) context;
        this.nutripdf = nutripdf;
    }

    @Override
    public int getCount() {
        return (urls == null) ? 0 : urls.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View gridItem = inflater.inflate(R.layout.grid_item,parent,false);

        ImageView imageView;
        final TextView textView;

        imageView = (ImageView)gridItem.findViewById(R.id.itemImageView);
        textView = (TextView)gridItem.findViewById(R.id.itemTextView);

        Picasso.with(context).load(urls.get(position)).into(imageView);
        textView.setText(names.get(position));

        myObj = new ArrayList<>();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myObj.add(objIds.get(position));
                mainActivity.getItemsNew(names.get(position), myObj, nutripdf);
            }
        });

        return gridItem;
    }
}
