package com.chamas.luis.churchs2.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.chamas.luis.churchs2.Adapters.ItemsPagerAdapter;
import com.chamas.luis.churchs2.Adapters.OfertasPagerAdapter;
import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class OfertasFragment extends Fragment {

    public static TextView startnum;
    public TextView finishnum;
    OfertasFragment ofertasFragment;
    ViewPager vpPager;
    MainActivity act;
    Button rightButton, leftButton;
    boolean nutripdf;

    OfertasPagerAdapter adapterViewPager;
    private FragmentActivity myContext;


    public OfertasFragment() {
        // Required empty public constructor
    }

    public OfertasFragment(boolean nutripdf){
        this.nutripdf = nutripdf;
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Ofertas");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Ofertas");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }

        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }

        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_ofertas, container, false);

        startnum = (TextView)rootview.findViewById(R.id.currentNumOfertas);
        finishnum = (TextView)rootview.findViewById(R.id.finalNumOfertas);
        leftButton = (Button)rootview.findViewById(R.id.leftOfertasButton);
        rightButton = (Button)rootview.findViewById(R.id.rightOfertasButton);


        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(vpPager.getCurrentItem() != 0){
                    vpPager.setCurrentItem(vpPager.getCurrentItem() - 1);
                }
            }
        });

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(vpPager.getCurrentItem() != OfertasPagerAdapter.NUM_ITEMS){
                    vpPager.setCurrentItem(vpPager.getCurrentItem() + 1);
                }
            }
        });

        vpPager = (ViewPager)rootview.findViewById(R.id.vpPager2);
        vpPager.setClipToPadding(false);
        vpPager.setPageMargin(65);
        vpPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                startnum.setText(String.valueOf(position+1));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        adapterViewPager = new OfertasPagerAdapter(act.getSupportFragmentManager());

        HashMap hashMap = new HashMap();

        ParseCloud.callFunctionInBackground("getOfertas", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
            @Override
            public void done(ArrayList<ParseObject> objects, ParseException e) {

                Log.d("hello", String.valueOf(objects));

                adapterViewPager.setData(objects, nutripdf);
                finishnum.setText(String.valueOf(OfertasPagerAdapter.NUM_ITEMS));

                vpPager.setAdapter(adapterViewPager);

            }

        });


//        vpPager.setPageTransformer(true, new DepthPageTransformer());




        return rootview;
    }
}
