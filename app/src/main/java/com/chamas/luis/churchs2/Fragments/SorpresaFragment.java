package com.chamas.luis.churchs2.Fragments;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.chamas.luis.churchs2.YoutubePlayerActivity;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class SorpresaFragment extends Fragment {

    private FragmentActivity myContext;
    MainActivity act;

    public SorpresaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("La Sorpresa");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }

    @Override
    public void onResume() {

        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("La Sorpresa");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }
        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }
//        activity.toolTitle.setText("Church");
        super.onDestroyView();
    }

    boolean nutripdf;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sorpresa, container, false);

        final TextView description, title, legal;
        final ImageView imageView;
        final ImageButton playButton;

        final RelativeLayout relativeLayout = (RelativeLayout)rootView.findViewById(R.id.relativeSorpresa);

        nutripdf = getArguments().getBoolean("nutripdf");

        ImageButton valorNutricional = (ImageButton)rootView.findViewById(R.id.sorpresaVerValorNutricionalButton);
        description = (TextView)rootView.findViewById(R.id.sorpresaDescriptionTextView);
        title = (TextView)rootView.findViewById(R.id.sorpresaTitleTextView);
        imageView = (ImageView)rootView.findViewById(R.id.sorpresaImageView);
        legal = (TextView)rootView.findViewById(R.id.sorpresaLegalTextView);

        playButton = (ImageButton)rootView.findViewById(R.id.sorpresaPlayButton);

        if(!nutripdf){
            valorNutricional.setVisibility(View.GONE);
        }

        HashMap hashMap = new HashMap();

        ParseCloud.callFunctionInBackground("getSorpresas", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void done(final ArrayList<ParseObject> object, ParseException e) {
                Log.d("sorpresa", String.valueOf(object));

                description.setText(object.get(0).getString("description"));
                title.setText(object.get(0).getString("name"));
                legal.setText(object.get(0).getString("legal"));

                if(object.get(0).getParseFile("image") == null){
                    Log.d("NULL", "Image Null");
                    String url = "http://img.youtube.com/vi/"+object.get(0).getString("videoUrl")+"/hqdefault.jpg";
//                    imageView.setVisibility(View.GONE);
                    Picasso.with(getActivity()).load(url).into(imageView);
                    playButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), YoutubePlayerActivity.class);
                            intent.putExtra("videoUrl", object.get(0).getString("videoUrl"));
                            startActivity(intent);

                        }
                    });
                }

                if(object.get(0).getString("videoUrl") == null || Objects.equals(object.get(0).getString("videoUrl"), "") || Objects.equals(object.get(0).getString("videoUrl"), " ")) {
                    Log.d("NULL", "Video Null");
                    Log.d("videoUrl", object.get(0).getString("videoUrl"));
                    playButton.setVisibility(View.GONE);
//                    relativeLayout.setBackgroundColor(Color.parseColor("#ffff"));
                    Picasso.with(getActivity()).load(object.get(0).getParseFile("image").getUrl()).into(imageView);
                }

                if((object.get(0).getString("videoUrl")!=null || !Objects.equals(object.get(0).getString("videoUrl"), "") || !Objects.equals(object.get(0).getString("videoUrl"), " ")) && object.get(0).getParseFile("image")!=null){
                    Log.d("NULL", "Both Null");
                    String url = "http://img.youtube.com/vi/"+object.get(0).getString("videoUrl")+"/hqdefault.jpg";
                    Picasso.with(getActivity()).load(url).into(imageView);
                    playButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), YoutubePlayerActivity.class);
                            intent.putExtra("videoUrl", object.get(0).getString("videoUrl"));
                            startActivity(intent);

                        }
                    });

                }


            }
        });

        return rootView;
    }

}
