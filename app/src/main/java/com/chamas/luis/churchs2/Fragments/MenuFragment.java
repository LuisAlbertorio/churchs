package com.chamas.luis.churchs2.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {


    private FragmentActivity myContext;

    public MenuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        MainActivity act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Menú");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }
        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }
//        activity.toolTitle.setText("Church");
        super.onDestroyView();
    }

    ImageView presotasImg;
    TextView presotasText;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_menu, container, false);
         presotasImg = (ImageView) rootView.findViewById(R.id.presotasImage);
         presotasText = (TextView)rootView.findViewById(R.id.presotasTextView);



        HashMap hashMap = new HashMap();

        ParseCloud.callFunctionInBackground("getCategories", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {

            @Override
            public void done(ArrayList<ParseObject> objects, ParseException e) {

                Picasso.with(getActivity()).load(objects.get(0).getParseFile("image").getUrl()).into(presotasImg);
                presotasText.setText(objects.get(0).getString("name"));
            }
        });


//        ((MainActivity)getActivity()).toolTitle.setText("Menu");


        return rootView;

    }


}
