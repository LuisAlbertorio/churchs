package com.chamas.luis.churchs2;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.File;

import es.voghdev.pdfviewpager.library.PDFViewPager;
import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.BasePDFPagerAdapter;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.asset.CopyAsset;
import es.voghdev.pdfviewpager.library.asset.CopyAssetThreadImpl;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;

public class pdfViewerActivity extends AppCompatActivity implements DownloadFile.Listener{

    WebView webView;
    PDFPagerAdapter adapter;
    RemotePDFViewPager remotePDFViewPager;
    PDFViewPager pdfViewPager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_viewer);

        Context context = this;
        DownloadFile.Listener listener = this;

//        pdfViewPager = (PDFViewPager) findViewById(R.id.pdfViewPager);



//        CopyAsset copyAsset = new CopyAssetThreadImpl(context, new Handler());
//        copyAsset.copy(asset, new File(getCacheDir(), "sample.pdf").getAbsolutePath());


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        webView = (WebView)findViewById(R.id.pdfView);

        WebSettings settings = webView.getSettings();

        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        settings.setBuiltInZoomControls(true);

        webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + getIntent().getStringExtra("url"));

        webView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {

            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                //Toast.makeText(this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }

        });



//        remotePDFViewPager = new RemotePDFViewPager(context, getIntent().getStringExtra("url"), listener);
//        remotePDFViewPager.setId(R.id.pdfViewPager);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        ((BasePDFPagerAdapter) pdfViewPager.getAdapter()).close();

    }


    @Override
    public void onSuccess(String url, String destinationPath) {
//        adapter = new PDFPagerAdapter(this, FileUtil.extractFileNameFromURL(url), remotePDFViewPager.getOffscreenPageLimit());
//        remotePDFViewPager.setAdapter(adapter);

//        Log.d("url", url);
//        Log.d("extract", String.valueOf(FileUtil.extractFileNameFromURL(url)));

    }

    @Override
    public void onFailure(Exception e) {

    }

    @Override
    public void onProgressUpdate(int progress, int total) {

    }
}
