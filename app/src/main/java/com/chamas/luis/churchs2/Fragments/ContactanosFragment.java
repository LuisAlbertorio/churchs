package com.chamas.luis.churchs2.Fragments;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.w3c.dom.Text;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactanosFragment extends Fragment {

    TextView llamanosText;
    private FragmentActivity myContext;
    Spinner pueblo;
    MainActivity act;
    EditText nombre, tel, email, msg;
    ImageButton enviar;

    public ContactanosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Contáctanos");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Contáctanos");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }
        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }
//        activity.toolTitle.setText("Church");
        super.onDestroyView();
    }

    TextView info;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_contactanos, container, false);

        ImageButton llamanos = (ImageButton)rootview.findViewById(R.id.llamanosButton);

        nombre = (EditText)rootview.findViewById(R.id.NameEditText);
        tel = (EditText)rootview.findViewById(R.id.TelEditText);
        email = (EditText)rootview.findViewById(R.id.EmailEditText);
        msg = (EditText)rootview.findViewById(R.id.MsgEditText);
        enviar = (ImageButton)rootview.findViewById(R.id.contactanosEnviarButton);
        info = (TextView)rootview.findViewById(R.id.contactanosInfoTextView);

        String[] items = {"Pueblo","Adjuntas", "Aguada", "Aguadilla", "Aguas Buenas", "Aibonito", "Añasco", "Arecibo", "Arroyo", "Barcelonta", "Barranquitas", "Bayamón", "Cabo Rojo", "Caguas", "Camuy", "Canóvanas", "Carolina", "Cataño", "Cayey", "Ceiba", "Ciales", "Cidra", "Coamo", "Comerío", "Corozal", "Dorado", "Fajardo", "Guayama", "Guánica", "Guayanilla", "Guaynabo", "Gurabo", "Hatillo", "Hormigueros", "Humacao", "Isabela", "Jayuya", "Juana Díaz", "Juncos", "Lajas", "Lares", "Las Marías", "Las Piedras", "Loíza", "Luquillo", "Manatí", "Maricao", "Maunabo", "Mayagüez", "Moca", "Morovis", "Naguabo", "Naranjito", "Orocovis", "Patillas", "Peñuelas", "Ponce", "Quebradillas", "Rincón", "Río Grande", "Sabana Grande", "Salinas", "San Germán", "San Juan", "San Lorenzo", "San Sebastian", "Santa Isabel", "Toa Alta", "Toa Baja", "Trujillo Alto", "Utuado", "Vega Alta", "Vega Baja", "Vieques", "Villalba", "Yabucoa", "Yauco"};

        pueblo = (Spinner)rootview.findViewById(R.id.pueblo);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);

        pueblo.setAdapter(adapter);

        ParseQuery<ParseObject> query = new ParseQuery<>("Copies");
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                info.setText(object.getString("contactanosInfo"));
            }
        });

        pueblo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(((TextView)parent.getChildAt(0)) != null){
                    ((TextView)parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        llamanos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:1-866-372-8246"));
                startActivity(intent);
            }
        });

        enviar.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                if(Objects.equals(nombre.getText().toString(), "") || Objects.equals(tel.getText().toString(), "") || Objects.equals(email.getText().toString(), "") || Objects.equals(msg.getText().toString(), "")){
                    Toast.makeText(getActivity(), "Por favor llenar todos los campos requeridos", Toast.LENGTH_LONG).show();
                }else{

                    ParseObject parseObject = new ParseObject("Contactanos");
                    parseObject.put("nombre", nombre.getText().toString());
                    parseObject.put("telefono", tel.getText().toString());
                    parseObject.put("email", email.getText().toString());
                    parseObject.put("platform", "android");

                    if (Objects.equals(pueblo.getSelectedItem().toString(), "pueblo")) parseObject.put("pueblo", null);
                    else parseObject.put("pueblo", pueblo.getSelectedItem().toString());

                    parseObject.put("mensaje", msg.getText().toString());

                    parseObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            act.graciasCont();
                        }
                    });

                }
            }
        });


        return rootview;


    }


}
