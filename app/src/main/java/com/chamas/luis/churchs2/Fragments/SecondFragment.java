package com.chamas.luis.churchs2.Fragments;


import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.chamas.luis.churchs2.R;
import com.chamas.luis.churchs2.YoutubePlayerActivity;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {

    // Store instance variables
    private String title;
    private int page;
    String oferta;
    ParseObject object;
    boolean nutripdf;

    public SecondFragment() {
        // Required empty public constructor
    }

    public SecondFragment(ParseObject object, boolean nutripdf){
        this.object = object;
        this.nutripdf = nutripdf;
    }


    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        page = getArguments().getInt("someInt", 0);
//        title = getArguments().getString("someTitle");
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_second, container, false);

        TextView tvLabel = (TextView)rootview.findViewById(R.id.tvLabel2);
        TextView description = (TextView)rootview.findViewById(R.id.ofertasDescriptionTextView);
        TextView legal = (TextView)rootview.findViewById(R.id.ofertasLegalTextView);
        ImageView imageView = (ImageView)rootview.findViewById(R.id.ofertasImageView);
        ImageButton valorNutricional = (ImageButton)rootview.findViewById(R.id.ofertasVerValorNutricionalButton);
        ImageButton playButton = (ImageButton)rootview.findViewById(R.id.ofertaPlayButton);

        if(!nutripdf){
            valorNutricional.setVisibility(View.GONE);
        }

        tvLabel.setText(object.getString("name"));
        description.setText(object.getString("description"));
        legal.setText(object.getString("legal"));

        if((object.getString("videoUrl")==null || Objects.equals(object.getString("videoUrl"), "") || Objects.equals(object.getString("videoUrl"), " ")) && object.getParseFile("image")==null) {
            playButton.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);

        }


            if((object.getString("videoUrl")==null || Objects.equals(object.getString("videoUrl"), "") || Objects.equals(object.getString("videoUrl"), " ")) && object.getParseFile("image") != null){
            Picasso.with(getActivity()).load(object.getParseFile("image").getUrl()).into(imageView);
            playButton.setVisibility(View.GONE);
        }

        if(object.getParseFile("image")==null){
            imageView.setVisibility(View.GONE);
            playButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), YoutubePlayerActivity.class);
                    intent.putExtra("videoUrl", object.getString("videoUrl"));
                    startActivity(intent);

                }
            });
        }

        if((object.getString("videoUrl")!=null || !Objects.equals(object.getString("videoUrl"), "") || !Objects.equals(object.getString("videoUrl"), " ")) && object.getParseFile("image")!=null){
            Picasso.with(getActivity()).load(object.getParseFile("image").getUrl()).into(imageView);
            playButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), YoutubePlayerActivity.class);
                    intent.putExtra("videoUrl", object.getString("videoUrl"));
                    startActivity(intent);

                }
            });
        }

            // Inflate the layout for this fragment

        return rootview;
    }

}
