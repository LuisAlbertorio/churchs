package com.chamas.luis.churchs2.Adapters;

import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.chamas.luis.churchs2.Fragments.FirstFragment;
import com.parse.ParseObject;

import java.util.ArrayList;

/**
 * Created by Luis on 6/16/2016.
 */

/**
 * IMPORTANT!! ALWAYS extend FragmentStatePagerAdapter NOT FragmentPagerAdapter
 */
public class ItemsPagerAdapter extends FragmentStatePagerAdapter{
    public static int NUM_ITEMS;

    ArrayList<ParseObject> items;
    boolean nutripdf;


    public ItemsPagerAdapter(FragmentManager childFragmentManager) {
        super(childFragmentManager);
    }


    @Override
    public float getPageWidth (int position) {
        return 0.93f;
    }

    public void setData(ArrayList<ParseObject> items, boolean nutripdf){
        this.items = items;
        this.nutripdf = nutripdf;
        NUM_ITEMS = items.size();
    }

//    public ItemsPagerAdapter(FragmentManager fragmentManager) {
//        super(fragmentManager);
//    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return (items == null) ? 0 : items.size();
    }

    // Returns the fragment to display for that page
    @Override
    public android.support.v4.app.Fragment getItem(int position) {

        return new FirstFragment(items.get(position), nutripdf);
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
//        super.restoreState(state, loader);
    }
}
