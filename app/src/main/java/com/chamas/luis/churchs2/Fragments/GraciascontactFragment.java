package com.chamas.luis.churchs2.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class GraciascontactFragment extends Fragment {

    private FragmentActivity myContext;
    MainActivity act;

    public GraciascontactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Contáctanos");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }


    @Override
    public void onResume() {
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Contáctanos");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }
        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }
//        activity.toolTitle.setText("Church");
        super.onDestroyView();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_graciascontact, container, false);
    }


}
