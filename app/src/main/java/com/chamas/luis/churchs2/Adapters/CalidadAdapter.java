package com.chamas.luis.churchs2.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.chamas.luis.churchs2.YoutubePlayerActivity;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Luis on 5/27/2016.
 */
public class CalidadAdapter extends BaseAdapter {

    Context context;
    private static LayoutInflater inflater=null;
    ArrayList<ParseObject> objects;

    public CalidadAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(ArrayList<ParseObject> objects){
        this.objects = objects;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return (objects == null) ? 0 : objects.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.calidad_grid_item, parent, false);

        TextView description = (TextView)rowView.findViewById(R.id.text);
        ImageButton play = (ImageButton)rowView.findViewById(R.id.calidadGridItemPlayButton);
//        description.setText("moo");
        description.setText(objects.get(position).getString("description"));
        ImageView imageView = (ImageView)rowView.findViewById(R.id.calidadPicVidOne);

        String url = "http://img.youtube.com/vi/"+objects.get(position).getString("videoUrl")+"/hqdefault.jpg";


        Picasso.with(context).load(url).into(imageView);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, YoutubePlayerActivity.class);
                intent.putExtra("videoUrl", objects.get(position).getString("videoUrl"));
                context.startActivity(intent);
            }
        });

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, YoutubePlayerActivity.class);
//                intent.putExtra("videoUrl", objects.get(position).getString("videoUrl"));
//                context.startActivity(intent);
            }
        });

        return rowView;    }
}
