package com.chamas.luis.churchs2.Fragments;


import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.chamas.luis.churchs2.Adapters.CalidadAdapter;
import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.chamas.luis.churchs2.YoutubePlayerActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class CalidadFragment extends Fragment {

    ImageButton playVideo;
    GridView gridView;
    CalidadAdapter caliAdapter;
    MainActivity act;
    ImageView headerImage;

    private FragmentActivity myContext;

    public CalidadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Calidad");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Calidad");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }

        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }
        super.onDestroyView();
    }

    public static String [] nameList={"Lorem ipsum dolor ultra video","Lorem ipsum dolor ultra video","Lorem ipsum dolor ultra video","Lorem ipsum dolor ultra video","Lorem ipsum dolor ultra video"};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootview = inflater.inflate(R.layout.fragment_calidad, container, false);

//        final TextView description = (TextView)rootview.findViewById(R.id.calidadHeaderDescriptionTextView);

        caliAdapter = new CalidadAdapter(getActivity());

//        playVideo = (ImageButton)rootview.findViewById(R.id.imageButton3);
        gridView = (GridView)rootview.findViewById(R.id.calidadGridView);
        gridView.setAdapter(caliAdapter);

        headerImage = (ImageView)rootview.findViewById(R.id.calidadMainImage);

        HashMap<String, Boolean> hashMap = new HashMap<>();
        hashMap.put("isHeader", false);

        ParseCloud.callFunctionInBackground("getCalidad", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {

            @Override
            public void done(ArrayList<ParseObject> object, ParseException e) {
                Log.d("cali", String.valueOf(object));
                caliAdapter.setData(object);
            }
        });

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Banners");
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                Picasso.with(getActivity()).load(object.getParseFile("bannerCalidad").getUrl()).into(headerImage);
            }
        });

//        HashMap<String, Boolean> headerHash = new HashMap<>();
//        headerHash.put("isHeader", true);
//
//        ParseCloud.callFunctionInBackground("getCalidad", headerHash, new FunctionCallback<ArrayList<ParseObject>>() {
//            @Override
//            public void done(final ArrayList<ParseObject> object, ParseException e) {
//
//                description.setText(object.get(0).getString("description"));
//
//                String url = "http://img.youtube.com/vi/"+object.get(0).getString("videoUrl")+"/hqdefault.jpg";
//
//                Picasso.with(getActivity()).load(url).into(headerImage);
//
//                playVideo.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        Intent intent = new Intent(getActivity(), YoutubePlayerActivity.class);
//                        intent.putExtra("videoUrl", object.get(0).getString("videoUrl"));
//                        getActivity().startActivity(intent);
//
//                    }
//                });
//            }
//        });




        return rootview;
    }

}
