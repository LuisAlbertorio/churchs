package com.chamas.luis.churchs2.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;

/**
 * Created by Luis on 5/30/2016.
 */
public class VideoAdapter extends BaseAdapter {

    MainActivity mainActivity2;
    Context context2;
    String [] result;
    private static LayoutInflater inflater=null;
    public VideoAdapter(MainActivity mainActivity, String[] name){
        result=name;
        context2 = mainActivity;
        mainActivity2 = mainActivity;
        inflater = (LayoutInflater)context2.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.custom_row_calidad, parent, false);

        TextView where = (TextView) rowView.findViewById(R.id.text);
//        TextView num = (TextView) rowView.findViewById(R.id.positionNumber);

        where.setText(result[position]);
//        num.setText(String.valueOf(position));

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                Toast.makeText(context2, "You Clicked " + result[position], Toast.LENGTH_LONG).show();

            }
        });

        return rowView;    }
}

