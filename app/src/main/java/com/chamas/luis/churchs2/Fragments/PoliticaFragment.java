package com.chamas.luis.churchs2.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PoliticaFragment extends Fragment {

    MainActivity act;
    private FragmentActivity myContext;

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Política de Privacidad");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Política de Privacidad");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }

        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }

        super.onDestroyView();
    }

    public PoliticaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_politica, container, false);




        return rootView;
    }

}
