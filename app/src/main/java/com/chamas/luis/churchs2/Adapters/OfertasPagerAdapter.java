package com.chamas.luis.churchs2.Adapters;

import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.chamas.luis.churchs2.Fragments.SecondFragment;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;

/**
 * Created by Luis on 6/16/2016.
 */

/**
 * IMPORTANT!! ALWAYS extend FragmentStatePagerAdapter NOT FragmentPagerAdapter
 */
public class OfertasPagerAdapter extends FragmentStatePagerAdapter {
    public static int NUM_ITEMS;

    public int num_item = 14;

    boolean nutripdf;

    ArrayList<ParseObject> objects;
//    SecondFragment secondFragment = new SecondFragment();

    public void setData(ArrayList<ParseObject> objects, boolean nutripdf){
        this.objects = objects;
        NUM_ITEMS = objects.size();
        this.nutripdf = nutripdf;
    }

    @Override
    public float getPageWidth (int position) {
        return 0.93f;
    }

    public OfertasPagerAdapter(FragmentManager fragmentManager) {super(fragmentManager);}

    // Returns total number of pages
    @Override
    public int getCount() {
        return (objects == null) ? 0 : objects.size();
    }

    // Returns the fragment to display for that page
    @Override
    public android.support.v4.app.Fragment getItem(int position) {

//        return secondFragment.newInstance(0, "Oferta Generica #" + String.valueOf(position + 1));
        Log.d("GET ITEM", "GETTING IT");
        return new SecondFragment(objects.get(position), nutripdf);
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
//        super.restoreState(state, loader);
    }
}
