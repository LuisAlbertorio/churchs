package com.chamas.luis.churchs2.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.chamas.luis.churchs2.Utilities.CustomSliderView;
import com.chamas.luis.churchs2.R;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    public MainFragment() {
        // Required empty public constructor
    }

    private SliderLayout mDemoSlider;
    ArrayList<String> urls;

    Context context;

    MainFragment myFragment;

    LinkedHashMap<String ,String> files;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragment = this;

        View rootview = inflater.inflate(R.layout.fragment_main, container, false);

        urls = new ArrayList<>();

        HashMap hashMap = new HashMap();


        mDemoSlider = (SliderLayout)rootview.findViewById(R.id.slider);

        mDemoSlider.setCustomIndicator((PagerIndicator)rootview.findViewById(R.id.custom_indicator));

        context = getActivity();

        ParseCloud.callFunctionInBackground("getHomeSlider", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
            @Override
            public void done(ArrayList<ParseObject> objects, ParseException e) {
                for (int i=0;i<objects.size();i++){
                    urls.add(objects.get(i).getParseFile("sliderImage").getUrl());

                    Log.d("slide url", urls.get(i));
                }

                files = new LinkedHashMap<>();

                for(int i=0;i<urls.size();i++){
                    files.put("file" + String.valueOf(i), urls.get(i));
                }

                for(String name : files.keySet()){
                        CustomSliderView customSliderView = new CustomSliderView(getActivity());
                        customSliderView
                                .image(files.get(name))
                                .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                                .setOnSliderClickListener(myFragment);

                    Log.d("FILES", String.valueOf(files.get(name)));


                    mDemoSlider.addSlider(customSliderView);
                }

                mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Tablet);
                mDemoSlider.setPadding(0,100,0,0);
                mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                mDemoSlider.setDuration(8000);
                mDemoSlider.addOnPageChangeListener(myFragment);
            }

        });

        return rootview;
    }

    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
//        Toast.makeText(getActivity(),"click",Toast.LENGTH_SHORT).show();
    }
}
