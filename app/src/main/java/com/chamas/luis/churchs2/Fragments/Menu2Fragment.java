package com.chamas.luis.churchs2.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.chamas.luis.churchs2.Adapters.MenuGridAdapter;
import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class Menu2Fragment extends Fragment {


    public Menu2Fragment() {
        // Required empty public constructor
    }


    private FragmentActivity myContext;
    MainActivity mainActivity;


    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        mainActivity = (MainActivity)activity;
        if (mainActivity.toolTitle.getVisibility() == View.INVISIBLE){
            mainActivity.toolTitle.setVisibility(View.VISIBLE);
            mainActivity.toolTitle.setText("Menú");
        }
        if (mainActivity.church.getVisibility() == View.VISIBLE){
            mainActivity.church.setVisibility(View.INVISIBLE);
        }
//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (mainActivity.toolTitle.getVisibility() == View.INVISIBLE){
            mainActivity.toolTitle.setVisibility(View.VISIBLE);
            mainActivity.toolTitle.setText("Menú");
        }
        if (mainActivity.church.getVisibility() == View.VISIBLE){
            mainActivity.church.setVisibility(View.INVISIBLE);
        }

        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }
        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }
//        activity.toolTitle.setText("Church");
        super.onDestroyView();
    }


    GridView gridView;
    boolean nutripdf;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_menu2, container, false);

        gridView = (GridView)rootView.findViewById(R.id.menuGridView);

        HashMap hashMap = new HashMap();

        nutripdf = getArguments().getBoolean("nutripdf");

        ParseCloud.callFunctionInBackground("getCategories", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
            @Override
            public void done(ArrayList<ParseObject> objects, ParseException e) {
                ArrayList<String> names = new ArrayList<String>();
                ArrayList<String> urls = new ArrayList<String>();
                ArrayList<String> objId = new ArrayList<String>();

                for(int i=0;i<objects.size();i++){
                    names.add(objects.get(i).getString("name"));
                    urls.add(objects.get(i).getParseFile("image").getUrl());
                    objId.add(objects.get(i).getObjectId());
                }

                gridView.setAdapter(new MenuGridAdapter(getActivity(), names, urls, objId, nutripdf));
            }
        });

        return rootView;
    }

}
