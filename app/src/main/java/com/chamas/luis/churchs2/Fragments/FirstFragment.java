package com.chamas.luis.churchs2.Fragments;


import android.annotation.TargetApi;
import android.net.LinkAddress;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chamas.luis.churchs2.R;
import com.parse.Parse;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {

    ParseObject object;
    boolean nutripdf;

    // Store instance variables
    private String title;
    private int page;


    public FirstFragment() {
        // Required empty public constructor
    }

    public FirstFragment(ParseObject object, boolean nutripdf){
        this.object = object;
        this.nutripdf = nutripdf;
    }

    // newInstance constructor for creating fragment with arguments
    public static FirstFragment newInstance(int page, String title ) {
        FirstFragment fragmentFirst = new FirstFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        page = getArguments().getInt("someInt", 0);
//        title = getArguments().getString("someTitle");
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_first, container, false);

        TextView calVal, proVal, fiVal;

        TextView titleLabel = (TextView)rootview.findViewById(R.id.tvLabel);
        TextView descTextView = (TextView)rootview.findViewById(R.id.descriptionTextView);
        ImageView imageView = (ImageView)rootview.findViewById(R.id.menuItemImageView);
        ImageButton valorNutricional = (ImageButton)rootview.findViewById(R.id.itemVerValorNutricional);
        calVal = (TextView)rootview.findViewById(R.id.caloriasValue);
        proVal = (TextView)rootview.findViewById(R.id.proteinasValue);
        fiVal = (TextView)rootview.findViewById(R.id.fibrasValue);

        LinearLayout nutricional, calories, fibra, proteinas;

        nutricional = (LinearLayout)rootview.findViewById(R.id.nutricionalValueBox);
        calories = (LinearLayout)rootview.findViewById(R.id.caloriasBox);
        fibra = (LinearLayout)rootview.findViewById(R.id.fibrasBox);
        proteinas = (LinearLayout)rootview.findViewById(R.id.proteinasBox);

        Picasso.with(getActivity()).load(object.getParseFile("image").getUrl()).into(imageView);

        if(object.getString("calories") == null || Objects.equals(object.getString("calories"), "") || Objects.equals(object.getString("calories"), " ")){
            calories.setVisibility(View.GONE);
            proVal.setText(object.getString("proteins"));
            fiVal.setText(object.getString("fiber"));
        }

        if(object.getString("fiber") == null || Objects.equals(object.getString("fiber"), "") || Objects.equals(object.getString("fiber"), " ")){
            fibra.setVisibility(View.GONE);
            calVal.setText(object.getString("calories"));
            proVal.setText(object.getString("proteins"));
        }

        if(object.getString("proteins") == null || Objects.equals(object.getString("proteins"), "") || Objects.equals(object.getString("proteins"), " ")){
            proteinas.setVisibility(View.GONE);
            calVal.setText(object.getString("calories"));
            fiVal.setText(object.getString("fiber"));
        }

        if ((object.getString("calories") == null || Objects.equals(object.getString("calories"), "") || Objects.equals(object.getString("calories"), " ")) && (object.getString("proteins") == null ||  Objects.equals(object.getString("proteins"), "") || Objects.equals(object.getString("proteins"), " ")) && (object.getString("fiber") == null) || Objects.equals(object.getString("fiber"), "") || Objects.equals(object.getString("fiber"), " ")){
            nutricional.setVisibility(View.GONE);
        }

        if (!nutripdf){
            valorNutricional.setVisibility(View.GONE);
        }

        titleLabel.setText(object.getString("name"));
        descTextView.setText(object.getString("description"));


        // Inflate the layout for this fragment
        return rootview;
    }


}
