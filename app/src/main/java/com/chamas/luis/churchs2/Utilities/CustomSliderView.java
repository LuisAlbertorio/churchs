package com.chamas.luis.churchs2.Utilities;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.chamas.luis.churchs2.R;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;

/**
 * Created by Luis on 3/21/2016.
 */
public class CustomSliderView extends BaseSliderView {

    private Bundle mBundle;

    public CustomSliderView(Context context){
        super(context);
    }

    public CustomSliderView bundle(Bundle bundle){
        mBundle = bundle;
        return this;
    }


    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    String mName;

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        this.mPrice = price;
    }

    String mPrice;


    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.custom_slider,null);
        ImageView target = (ImageView)v.findViewById(R.id.daimajia_slider_image);
        bindEventAndShow(v, target);
        return v;
    }

    public CustomSliderView info(String name, String price){
        mName = name;
        mPrice = price;
        return this;
    }

}
