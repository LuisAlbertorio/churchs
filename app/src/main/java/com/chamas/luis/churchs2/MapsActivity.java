package com.chamas.luis.churchs2;

import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    double lat, lon;
    TextView name, phone, address, distance;
    TextView lunes, martes, miercoles, jueves, viernes, sabado, domingo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        name = (TextView)findViewById(R.id.mapsPlaceName);
        phone = (TextView)findViewById(R.id.mapsPhone);
        address = (TextView)findViewById(R.id.mapsAddress);
        distance = (TextView)findViewById(R.id.mapsDistance);
        lunes = (TextView)findViewById(R.id.mapsLunes);
        martes = (TextView)findViewById(R.id.mapsMartes);
        miercoles = (TextView)findViewById(R.id.mapsMiercoles);
        jueves = (TextView)findViewById(R.id.mapsJueves);
        viernes = (TextView)findViewById(R.id.mapsViernes);
        sabado = (TextView)findViewById(R.id.mapsSabados);
        domingo = (TextView)findViewById(R.id.mapsDomingo);

        name.setText(getIntent().getExtras().getString("name"));
        phone.setText(getIntent().getExtras().getString("phone"));
        address.setText(getIntent().getExtras().getString("address"));
        distance.setText(getIntent().getExtras().getString("distance") + " millas");

        lunes.setText(getIntent().getExtras().getString("lunes"));
        martes.setText(getIntent().getExtras().getString("martes"));
        miercoles.setText(getIntent().getExtras().getString("miercoles"));
        jueves.setText(getIntent().getExtras().getString("jueves"));
        viernes.setText(getIntent().getExtras().getString("viernes"));
        sabado.setText(getIntent().getExtras().getString("sabado"));
        domingo.setText(getIntent().getExtras().getString("domingo"));

        lat = getIntent().getExtras().getDouble("lat");
        lon = getIntent().getExtras().getDouble("lon");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng church = new LatLng(lat,lon);
        mMap.addMarker(new MarkerOptions().position(church).icon(BitmapDescriptorFactory.fromResource(R.drawable.churchsmallerrrrrrrr)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(church, 15.5f));
    }

}
