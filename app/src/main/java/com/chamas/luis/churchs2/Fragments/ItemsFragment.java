package com.chamas.luis.churchs2.Fragments;


import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.chamas.luis.churchs2.Adapters.ItemsPagerAdapter;
import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class ItemsFragment extends Fragment {

    ItemsPagerAdapter adapterViewPager;
    private FragmentActivity myContext;
    TextView titleOfCat;
    public static TextView startnum;
    public TextView finishnum;
    Button rightButton, leftButton;
    ViewPager vpPager ;
    String catName;
    ArrayList<String> objId;
    MainActivity act;
    boolean nutripdf;


    public ItemsFragment(String catName, ArrayList<String> objId, boolean nutripdf){
        this.catName = catName;
        this.objId = objId;
        this.nutripdf = nutripdf;
    }

    public ItemsFragment() {
        // Required empty public constructor
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Menú");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }

//        adapterViewPager = new ItemsPagerAdapter(getChildFragmentManager());

//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Menú");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }

        Log.d("onResume", "onResume");

        super.onResume();
    }

    @Override
    public void onDestroyView() {

//        MainActivity activity = (MainActivity)myContext;
//        if (activity.toolTitle.getVisibility() == View.VISIBLE){
//            activity.toolTitle.setVisibility(View.INVISIBLE);
//            activity.toolTitle.setText("");
//        }
//        if (activity.church.getVisibility() == View.INVISIBLE){
//            activity.church.setVisibility(View.VISIBLE);
//        }
//        activity.toolTitle.setText("Church");
        super.onDestroyView();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_items, container, false);

        Log.d("onCreate", "onCreate");


        titleOfCat = (TextView)rootview.findViewById(R.id.titleOfCat);
        startnum = (TextView)rootview.findViewById(R.id.currentNumItems);
        finishnum = (TextView)rootview.findViewById(R.id.finalNumItems);
        rightButton = (Button)rootview.findViewById(R.id.rightButton);
        leftButton = (Button)rootview.findViewById(R.id.leftButton);

//        finishnum.setText(String.valueOf(ItemsPagerAdapter.NUM_ITEMS));

        titleOfCat.setText(catName);

        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(vpPager.getCurrentItem() != 0){

                    vpPager.setCurrentItem(vpPager.getCurrentItem() - 1);

                }
            }
        });

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(vpPager.getCurrentItem() != ItemsPagerAdapter.NUM_ITEMS - 1){

                    vpPager.setCurrentItem(vpPager.getCurrentItem() + 1);

                }
            }
        });

        vpPager = (ViewPager)rootview.findViewById(R.id.vpPager);
        vpPager.setClipToPadding(false);
        vpPager.setPageMargin(65);
        vpPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                startnum.setText(String.valueOf(position+1));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        adapterViewPager = new ItemsPagerAdapter(myContext.getSupportFragmentManager());

//        vpPager.setAdapter(adapterViewPager);

        Log.d("objID", String.valueOf(objId));

        HashMap<String, ArrayList<String>> hashMap = new HashMap<>();
        hashMap.put("categories", objId);


        ParseCloud.callFunctionInBackground("getMenu", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
            @Override
            public void done(ArrayList<ParseObject> objects, ParseException e) {

                Log.d("hello", String.valueOf(objects));
                adapterViewPager.setData(objects, nutripdf);

                finishnum.setText(String.valueOf(ItemsPagerAdapter.NUM_ITEMS));

                vpPager.setAdapter(adapterViewPager);
            }
        });

        return rootview;
    }

//    public static class MyPagerAdapter extends FragmentPagerAdapter {
//        private static int NUM_ITEMS = 14;
//
//        @Override
//        public float getPageWidth (int position) {
//            return 0.93f;
//        }
//
//        public MyPagerAdapter(FragmentManager fragmentManager) {
//            super(fragmentManager);
//        }
//
//        // Returns total number of pages
//        @Override
//        public int getCount() {
//            return NUM_ITEMS;
//        }
//
//        // Returns the fragment to display for that page
//        @Override
//        public android.support.v4.app.Fragment getItem(int position) {
////            switch (position) {
////                case 0: // Fragment # 0 - This will show FirstFragment
////                    return FirstFragment.newInstance(0, "3 Presas de Pollo");
////                case 1: // Fragment # 0 - This will show FirstFragment different title
////                    return FirstFragment.newInstance(1, "3 Presas de Pollo");
////                case 2: // Fragment # 1 - This will show SecondFragment
////                    return FirstFragment.newInstance(2, "3 Presas de Pollo");
////                default:
////                    return null;
////            }
//
//            return FirstFragment.newInstance(0, "3 Presas de pollo");
//        }
//
//        // Returns the page title for the top indicator
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return "Page " + position;
//        }
//
//    }


}
