package com.chamas.luis.churchs2.Utilities;

import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by Luis on 6/27/2016.
 */
public class Application extends android.app.Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId( "aDlgQUrkOStzqxP8FHV19wU2aLqNjWzAAssRvyca")
                .clientKey("NHLAY9eCaJvmWFrvbJu9MvHUImFZpE6hBioWpt2e")
                .server("https://parseapi.back4app.com/")
                .build());

        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("GCMSenderId", "794638389859");
        installation.saveInBackground();
    }
}
