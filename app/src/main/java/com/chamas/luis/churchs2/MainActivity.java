package com.chamas.luis.churchs2;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.chamas.luis.churchs2.Fragments.CalidadFragment;
import com.chamas.luis.churchs2.Fragments.CondicionesFragment;
import com.chamas.luis.churchs2.Fragments.ContactanosFragment;
import com.chamas.luis.churchs2.Fragments.GraciasSubFragment;
import com.chamas.luis.churchs2.Fragments.GraciascontactFragment;
import com.chamas.luis.churchs2.Fragments.ItemsFragment;
import com.chamas.luis.churchs2.Fragments.LocaleFragment;
import com.chamas.luis.churchs2.Fragments.LocalizadorFragment;
import com.chamas.luis.churchs2.Fragments.MainFragment;
import com.chamas.luis.churchs2.Fragments.Menu2Fragment;
import com.chamas.luis.churchs2.Fragments.MenuFragment;
import com.chamas.luis.churchs2.Fragments.NutricionFragment;
import com.chamas.luis.churchs2.Fragments.OfertasFragment;
import com.chamas.luis.churchs2.Fragments.PoliticaFragment;
import com.chamas.luis.churchs2.Fragments.QuienesFragment;
import com.chamas.luis.churchs2.Fragments.SorpresaFragment;
import com.chamas.luis.churchs2.Fragments.SuscribeteFragment;
import com.chamas.luis.churchs2.Fragments.VideosFragment;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.parse.ParsePush;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    /*
        Initialize all the fragments to be used,
     */
    Fragment mainFragment = new MainFragment();
    Fragment menuFragment = new MenuFragment();
    Fragment menu2Fragment = new Menu2Fragment();
    Fragment contactanosFragment = new ContactanosFragment();
    Fragment graciascontactFragment = new GraciascontactFragment();
    Fragment quienesFragment = new QuienesFragment();
    Fragment suscribeteFragment = new SuscribeteFragment();
    Fragment nutricionFragment = new NutricionFragment();
    Fragment localizadorFragment = new LocalizadorFragment();
    Fragment calidadFragment = new CalidadFragment();
    Fragment itemsFragment = new ItemsFragment();
    Fragment sorpresaFragment = new SorpresaFragment();
    Fragment graciasSubFragment = new GraciasSubFragment();
    Fragment videoFragment = new VideosFragment();

    boolean nutripdf;

    boolean fromNav;

    public FragmentTransaction ft;
    public TextView toolTitle;
    public ImageView church;

    public FragmentManager fragmentManager;
    String catName;

    NavigationView navigationView;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolTitle = (TextView)findViewById(R.id.titleOfMyTool);
        church =(ImageView)findViewById(R.id.imageView);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        nutripdf = getIntent().getExtras().getBoolean("nutripdf");

         fromNav = false;

        Log.d("nutripdf", String.valueOf(getIntent().getExtras().getBoolean("nutripdf")));

        /*
            Set the Home Fragment
         */
        ft = getFragmentManager().beginTransaction();
        ft.add(R.id.frameLayout, mainFragment);
        ft.commit();

        fragmentManager = getSupportFragmentManager();

    }

    /*
        Launch menu when menu button in main fragment is pressed, The other functions
        are pretty much the same
     */
    public void launchMenu(View view) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("nutripdf", nutripdf);

        menu2Fragment.setArguments(bundle);

        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, menu2Fragment, "MENU");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
    }


    public void contactanos(View view) {

        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, contactanosFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
    }

    public void quienesSomos(View view){

        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, quienesFragment, "QUIENES");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
    }

    public void launchMap(View view) {

        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, localizadorFragment, "LOCAL");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
    }


    public void calidad(View view) {

        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, calidadFragment, "CALIDAD");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
    }

    /*
        Called from the menu fragment, "categories"
     */
    public void getItems(View view) {

        Bundle bundle = new Bundle();

        switch (view.getId()){
            case R.id.especialidadesButton:
                bundle.putString("title", "Especialidades");
                itemsFragment.setArguments(bundle);
                catName = "Especialidades";
                break;
            case R.id.combofamButton:
                bundle.putString("title", "Ofertas Familiares");
                itemsFragment.setArguments(bundle);
                catName = "Ofertas Familiares";
                break;
            case R.id.complementosButton:
                bundle.putString("title", "Ricos Complementos");
                itemsFragment.setArguments(bundle);
                catName = "Ricos Complementos";
                break;
            case R.id.sorpresaButton:
                bundle.putString("title", "La Sorpresa");
                itemsFragment.setArguments(bundle);
                catName = "La Sorpresa";
                break;
            case R.id.bebidasButton:
                bundle.putString("title", "Bebidas");
                itemsFragment.setArguments(bundle);
                catName = "Bebidas";
                break;
            case R.id.postresButton:
                bundle.putString("title", "Sabrosos Postres");
                itemsFragment.setArguments(bundle);
                catName = "Sabrosos Postres";
                break;
            case R.id.presotasButton:
                bundle.putString("title", "Presotas Frescas");
                itemsFragment.setArguments(bundle);
                catName = "Presotas Frescas";
                break;
            case R.id.sandwichesButton:
                bundle.putString("title", "Sandwiches & Wraps");
                itemsFragment.setArguments(bundle);
                catName = "Sandwiches & Wraps";
                break;
            case R.id.chavitoButton:
                bundle.putString("title", "Menú del Chavito");
                itemsFragment.setArguments(bundle);
                catName = "Menú del Chavito";
                break;
            case R.id.desayunoButton:
                bundle.putString("title", "Fresco Desayuno");
                itemsFragment.setArguments(bundle);
                catName = "Fresco Desayuno";
                break;
        }

//        ft = getFragmentManager().beginTransaction();
//        ft.replace(R.id.frameLayout, new ItemsFragment(catName), "ITEMS");
//        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        ft.addToBackStack(null);
//        ft.commit();
    }

     /*
        Called from the menu fragment, "categories"
     */
    public void getItemsNew(String name, ArrayList<String> objId, boolean nutripdf){

        Bundle bundle = new Bundle();
        bundle.putStringArrayList("objId", objId);
        bundle.putBoolean("nutripdf", nutripdf);

        catName = name;

        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, new ItemsFragment(catName, objId, nutripdf), "ITEMS");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }

    //called from contactanos fragment
    public void graciasCont() {


        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, graciascontactFragment, "GRACIASCONT");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);

        for(int i=0;i<getFragmentManager().getBackStackEntryCount();i++){
            getFragmentManager().popBackStack();
        }

        ft.commit();
    }


    //called from Gracias contact fragment
    public void regresarFromGraciasCont(View view) {
        ft = getFragmentManager().beginTransaction();
        for(int i=0; i<getFragmentManager().getBackStackEntryCount(); i++){
            getFragmentManager().popBackStack();
        }
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        ft.replace(R.id.frameLayout, mainFragment);
        ft.commit();
    }

    public void sorpresabutton(View view) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("nutripdf", nutripdf);
        sorpresaFragment.setArguments(bundle);

        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, sorpresaFragment, "SORPRESA");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
    }

    public void graciasSub() {
        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, graciasSubFragment, "GRACIASSUB");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        for(int i=0; i<getFragmentManager().getBackStackEntryCount(); i++){
            getFragmentManager().popBackStack();
        }
        ft.commit();

    }


    public void launchOfertas(View view) {

        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, new OfertasFragment(nutripdf), "OFERTAS");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
    }

    /*
        Method to open the navigation drawer
     */
    public void navDraw(View view) {
        drawer.openDrawer(GravityCompat.END);
    }


    /*
        Methods called when an item in the navigation drawer is clicked
     */
    public void navDrawerItem(View view){

        fromNav = true;

        switch (view.getId()){
            case R.id.calidadNav:
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, calidadFragment, "CALIDAD");
//                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;
            case R.id.menuNav:

                if(getFragmentManager().findFragmentByTag("MENU") == null){
                    Bundle bundle = new Bundle();

                    bundle.putBoolean("nutripdf", nutripdf);
                    menu2Fragment.setArguments(bundle);

                    ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, menu2Fragment, "MENU");
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    ft.addToBackStack(null);
                    ft.commit();

                    if (drawer.isDrawerOpen(GravityCompat.END)) {
                        drawer.closeDrawer(GravityCompat.END);
                    }
                }else{
                    ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.frameLayout, menu2Fragment, "MENU");
                    ft.addToBackStack(null);
                    ft.commit();

                    if (drawer.isDrawerOpen(GravityCompat.END)) {
                        drawer.closeDrawer(GravityCompat.END);
                    }
                }

                break;
            case R.id.ofertasNav:
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, new OfertasFragment(nutripdf), "OFERTAS");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;
            case R.id.sorpresaNav:
                Bundle bundle1 = new Bundle();
                bundle1.putBoolean("nutripdf", nutripdf);
                sorpresaFragment.setArguments(bundle1);

                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, sorpresaFragment, "SORPRESA");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;
            case R.id.localizadorNav:
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, localizadorFragment, "LOCALIZADOR");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;
            case R.id.quienesNav:
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, quienesFragment, "QUIENES");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;
            case R.id.nutricionNav:
                Bundle bundle2 = new Bundle();
                bundle2.putBoolean("nutripdf", nutripdf);
                nutricionFragment.setArguments(bundle2);

                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, nutricionFragment, "NUTRICION");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;
            case R.id.videosNav:
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, videoFragment, "VIDEO");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;
            case R.id.suscribeteNav:
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, suscribeteFragment, "SUSCRIBETE");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;
            case R.id.contactanosNav:
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, contactanosFragment, "CONTACTANOS");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;

            case R.id.politicaNav:
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, new PoliticaFragment(), "POLITICA");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;

            case R.id.condicionesNav:
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frameLayout, new CondicionesFragment(), "CONDICIONES");
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();

                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }

                break;
        }
    }

    /*
        Method callled when the valor nutricional button is
        pressed, will take you to nutricion fragment so you
        can then see pdf
     */
    public void VerValorNutricional(View view) {
        Bundle bundle2 = new Bundle();
        bundle2.putBoolean("nutripdf", nutripdf);
        nutricionFragment.setArguments(bundle2);

        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, nutricionFragment, "NUTRICION");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView)findViewById(R.id.nav_view);
        GraciascontactFragment gf = (GraciascontactFragment)getFragmentManager().findFragmentByTag("GRACIASCONTACT");

        /*
            If navegation drawer is opened then closed it
         */
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }

        if(fromNav){
            for (int i=0;i<getFragmentManager().getBackStackEntryCount() - 1; i++){
                getFragmentManager().popBackStack();
            }
            fromNav=false;
        }

        /*
            Pop backstack as long as its not empty
         */
        if (getFragmentManager().getBackStackEntryCount() != 0){
            getFragmentManager().popBackStack();
        }else{
            super.onBackPressed();
        }
    }


}
