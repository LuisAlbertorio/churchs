package com.chamas.luis.churchs2;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class SplashActivity extends AppCompatActivity {


    boolean nutripdf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //Set notification bar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }


        //Provides a pause of 2 second
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Nutricion");
                parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject object, ParseException e) {
                        if(object.getParseFile("pdf") == null){
                            nutripdf = false;
                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                            intent.putExtra("nutripdf", nutripdf);
                            startActivity(intent);
                            finish();
                        }else{
                            nutripdf = true;
                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                            intent.putExtra("nutripdf", nutripdf);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
            }
        },2000);

    }
}
