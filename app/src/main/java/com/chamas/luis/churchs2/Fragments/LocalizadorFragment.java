package com.chamas.luis.churchs2.Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.chamas.luis.churchs2.Adapters.ItemsPagerAdapter;
import com.chamas.luis.churchs2.Adapters.LocalizadorListAdapter;
import com.chamas.luis.churchs2.MainActivity;
import com.chamas.luis.churchs2.R;
import com.parse.FunctionCallback;
import com.parse.LocationCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;


import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;


/**
 * A simple {@link Fragment} subclass.
 */
public class LocalizadorFragment extends Fragment {
    ListView lv;
    LocalizadorListAdapter locaListAdapter;
    Location loc;
    ParseGeoPoint geo;
    MainActivity act;

    private FragmentActivity myContext;

    public LocalizadorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        act = (MainActivity)activity;
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Localizador");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
//        act.toolTitle.setText("Contactanos");
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        if (act.toolTitle.getVisibility() == View.INVISIBLE){
            act.toolTitle.setVisibility(View.VISIBLE);
            act.toolTitle.setText("Localizador");
        }
        if (act.church.getVisibility() == View.VISIBLE){
            act.church.setVisibility(View.INVISIBLE);
        }
        super.onResume();
    }

    @Override
    public void onDestroyView() {

        MainActivity activity = (MainActivity)myContext;
        if (activity.toolTitle.getVisibility() == View.VISIBLE){
            activity.toolTitle.setVisibility(View.INVISIBLE);
            activity.toolTitle.setText("");
        }
        if (activity.church.getVisibility() == View.INVISIBLE){
            activity.church.setVisibility(View.VISIBLE);
        }
//        activity.toolTitle.setText("Church");
        super.onDestroyView();
    }

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;

    private static final int LOCATION_REQUEST = INITIAL_REQUEST + 3;

    ProgressDialog progressDialog;

    LocationTracker tracker;


    public void doLocationThing() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        tracker = new LocationTracker(getActivity(),
                new TrackerSettings()
                    .setUseGPS(true)
                    .setUseNetwork(true)
        ) {
            @Override
            public void onLocationFound(final Location location) {
                // Do some stuff
                progressDialog.dismiss();

                geo = new ParseGeoPoint();
                geo.setLatitude(location.getLatitude());
                geo.setLongitude(location.getLongitude());

                HashMap<String, ParseGeoPoint> hashMap = new HashMap<>();
                hashMap.put("userLocation", geo);

                ParseCloud.callFunctionInBackground("getLocalidades", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
                    @Override
                    public void done(ArrayList<ParseObject> objects, ParseException e) {
                        Log.d("getLocal", String.valueOf(objects));
                        locaListAdapter.setData(objects, location);

//                        lv.setAdapter(locaListAdapter);
                    }
                });

            }

            @Override
            public void onTimeout() {

            }
        };
        tracker.startListening();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST) {
            if (canAccessLocation()) {
                doLocationThing();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }


    private boolean hasPermission(String perm) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (PackageManager.PERMISSION_GRANTED == getActivity().checkSelfPermission(perm));
        }
        return false;
    }


    @Override
    public void onPause() {
        if(tracker != null) {
            tracker.stopListening();
        }
        super.onPause();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_localizador, container, false);

        Log.d("progreess", "pro");

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Getting location...");
        progressDialog.show();

        locaListAdapter = new LocalizadorListAdapter(getActivity());

        lv = (ListView)rootView.findViewById(R.id.localListview);

        lv.setAdapter(locaListAdapter);


        // You can pass an ui Context but it is not mandatory getApplicationContext() would also works
        // Be aware if you target android 23, you'll need to handle the runtime-permissions !
        // see http://developer.android.com/reference/android/support/v4/content/ContextCompat.html
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // You need to ask the user to enable the permissions
//            Toast.makeText(this, "ask user permission", Toast.LENGTH_LONG).show();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }

        } else {
            doLocationThing();
        }



        ParseGeoPoint parseGeo = new ParseGeoPoint(18.2383446,-66.0500762);


//        Criteria criteria = new Criteria();
//        criteria.setAccuracy(Criteria.ACCURACY_LOW);
//
//        ParseGeoPoint.getCurrentLocationInBackground(25000, criteria,new LocationCallback() {
//            @Override
//            public void done(ParseGeoPoint geoPoint, ParseException e) {
//                if(e==null){
//                    geo = geoPoint;
//                    loc = new Location("");
//                    loc.setLatitude(geo.getLatitude());
//                    loc.setLongitude(geo.getLongitude());
//
//                    HashMap<String, ParseGeoPoint> hashMap = new HashMap<>();
//                    hashMap.put("userLocation", geo);
//
//
//                    locaListAdapter = new LocalizadorListAdapter((MainActivity)getActivity());
//
//                    ParseCloud.callFunctionInBackground("getLocalidades", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
//                        @Override
//                        public void done(ArrayList<ParseObject> objects, ParseException e) {
//                            Log.d("getLocal", String.valueOf(objects));
//                            locaListAdapter.setData(objects, loc);
//                            lv.setAdapter(locaListAdapter);
//                        }
//                    });
//
//                }else Log.d("error", String.valueOf(e));
//
//            }
//        });






//        lv.setAdapter(new LocalizadorListAdapter((MainActivity)getActivity(), nameList));

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog.isShowing()) progressDialog.dismiss();

    }
}
